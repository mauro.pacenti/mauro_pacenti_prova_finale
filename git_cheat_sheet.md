# Git cheat sheet

## Per cominciare
Impostazioni iniziali come credenziali, directory di partenza e stato.

|Comando|Spiegazione|
|-|-|
|git init|Inizializza la cartella di lavoro, creando la cartella .git al suo interno, rendendola quindi una repository.|
|git config (--global) user.name *nome cognome*|Configura il nome utente, scrivendo *nome cognome* nel file .git/config. Con --global si modifica il file $HOME/.gitconfig.|
|git config (--gloabl) user.email *indirizzo email*|Configura il nome utente, scrivendo *indirizzo email* nel file .git/config. Con --global si modifica il file $HOME/.gitconfig.|
|git status|Mostra lo stato attuale della repository, indicando se ci sono state modifiche che siano pronte per il commit o no.|

## Commit
Cuore pulsante di git, gestiscono le modifiche apportate alla repository.

|Comando|Spiegazione|
|-|-|
|git add *nomeFile*|Aggiunge nomeFile ai file che verranno committati al prossimo comando "commit" (verde=aggiunto , rosso=non aggiunto).|
|git commit *nomeFile*|Committa nomeFile, rendendo la modifica effettiva e richiedendo un commit message.|
|git show (*commitId*)|Mostra l'ultimo commit effettuato con relativo commit message. Con commit id mostra il suddetto commit.|
|git log (--pretty)|Mostra tutto l'elenco dei commit con relativi commit message. Con --pretty|
|git diff (*commitId1*) (*commitId2*) or (*nomeBranch1*) (*nomeBranch2*)|Mostra le differenze tra lo stato attuale dei file non aggiunti e l'ultimo commit. Con le aggiunte *commitId1* (bastano i primi 8 caratteri!) o *nomeBranch1* si possono confrontare i suddetti con quelli attuali, inserendone un secondo invece il confronto viene fatto sulla differenza.|
|git checkout *commitId*|Riporta la repository alla stato di *commitId*.|
|git stash|Salva le modifiche non committate e le rimuove dal branch corrente|
   
## Branch
Letteralmente rappresentano i "rami" dell'albero di sviluppo della repository. Il principale si chiama "master".

|Comando|Spiegazione|
|-|-|
|git branch|Mostra tutti i branch attaccati alla repository.|
|git checkout (-b) *nomeBranch*|Si sposta sul branch *nomeBranch*. Con -b, viene preventivamente creato un branch nominato *nomeBranch*.|
|git merge *nomeBranch*|Aggiunge al branch attuale *nomeBranch*, appendendolo dopo l'ultimo commit.|

## Remote repositories
Parte di git orientata al lavoro condiviso e salvato in remoto.

|Comando|Spiegazione|
|-|-|
|git remote add *alias* *url*|aggiunge l'*url* delal respository remota, assegnandole un *alias*.|
|git fetch *alias*|scarica tutti i nuovi commit della repository identificata con *alias*.|
|git pull *alias*|esegue il commando fetch e successivamente il comando merge.|
|git push *alias* *branch*|trasferisce il branch locale al *branch* della repository remota *alias*.|

## Regole di etichetta per git
Alcune regole di buona condotta e consigli per l'utilizzo di git.
|Pratica|Descrizione|
|-|-|
|Git status dopo ogni operazione|Usare git status costantemente aiuta il monitoraggio delle operazioni in corso.|
|Commit significativi|Ogni commit dovrebbe avere una descrizione sintetica ma emblematica delle modifiche apportate.|
|Denominazione branch *feat/* e *fix/*|Nel creare un nuovo branch è buona prassi organizzarli secondo feature o fix, in base alla natura del branch. Così che possano essere più facilmente identificati a colpo d'occhio.|
|Merge e merge request|Nel momento in cui ci si rapporta con una repository remota, è normale affrontare situazioni in cui si lavori in gruppo. Quando qualcuno vuole innestare il branch a cui a lavorato sul master, è necessario effettuare una push (MAI SUL MASTER) seguita da una merge request. Si procede quindi all'operazione di merge tramite gitlab. In alternativa è possibile gestire l'operazione di merge in locale.|
|Gestione dei conflitti|Sicuramente la parte più difficile di git da affrontare, si presentano quando durante l'operazione di merge vi è un incongruenza tra lo stato dei file sul master e quello dei file sul branch. Tuttavia, la difficoltà intrinseca dei conflitti verticalizza sulle capacità di comprensione del codice, piuttosto che di git.| 
